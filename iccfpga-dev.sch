EESchema Schematic File Version 4
LIBS:iccfpga-dev-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2150 2850 1500 1500
U 5AEC4D13
F0 "power" 60
F1 "power.sch" 60
$EndSheet
$Sheet
S 5550 2850 1500 1500
U 5AEC6E15
F0 "fpga" 60
F1 "fpga.sch" 60
$EndSheet
$Sheet
S 7250 2850 1500 1500
U 5AEC91D6
F0 "cp2102usb" 60
F1 "cp2102usb.sch" 60
$EndSheet
$Sheet
S 3850 2850 1500 1500
U 5AECA943
F0 "rpigpio" 60
F1 "rpigpio.sch" 60
$EndSheet
$EndSCHEMATC
